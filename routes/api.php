<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix'=>'tables','namespace'=>'Restaurants\Tables'],function(){
    Route::name('tables.')->group(function(){
        Route::post('createtable',['as'=>'createtable','uses'=>'TableController@CreateTable']);
        Route::delete('deletetable/{identificator}',['as'=>'deletetable','uses'=>'TableController@DeleteTable']);

    });
});
Route::group(['prefix'=>'reservations','namespace'=>'Restaurants\Reservations'],function(){
    Route::name('reservations.')->group(function(){
        Route::post('createreservation',['as'=>'createreservation','uses'=>'ReservationController@CreateReservation']);
        Route::get('checktablesreservation/{reserve_date}/{persons}',['as'=>'checktablesreservation','uses'=>'ReservationController@CheckTableReservationPerDate']);
        Route::delete('deletereservation/{reserve_number}',['as'=>'deletereservation','uses'=>'ReservationController@DeleteReserve']);
    });
});