<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 27/3/2021
 * Time: 6:41 PM
 */

namespace App\Http\Functionalities;


use App\Models\Reservation;

class ReserveFunctionalities
{
    public function CheckTableReservePerIdTable($tableIdenficator)
    {
        $checkReserve = Reservation::where('table_id','=',$tableIdenficator)
            ->count();
        return $checkReserve;
    }
}