<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 27/3/2021
 * Time: 5:34 PM
 */

namespace App\Http\Validations;


use Illuminate\Support\Facades\Validator;

class TableValidations
{
    public function ValidateCreateTable($request)
    {
        $capMin = $request->cap_min;
        $capMax = $request->cap_max;
        $errores = array();

        /*Using this way we validate that identificator is unique and isn't empty*/
        $data = $request->all();

        $rules = array
        (
            'identificator' => 'required|unique:tables,identificator',
        );

        $messages = array
        (
            'identificator.required' => 'The table identification number is necessary',
            'identificator.unique' => 'The identification number of the table you are trying to register already exists, check and try again',
        );

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails())
        {
            return $v->errors();
        }
        else
        {
            /*Using this other way we can create our own valid options*/
            if ($capMin <= 0 || empty($capMin) || !is_numeric($capMin))
            {
                $errores = ['cap_min' => ['The minimum capacity cannot be less than zero, be empty or not be a number']];
            }
            else
                if ($capMax <= 0 || empty($capMax) || !is_numeric($capMax))
                {
                    $errores = ['cap_max' => ['The maximum capacity cannot be less than zero, be empty or not be a number']];
                }
                else
                    if ($capMax <= $capMin)
                    {
                        $errores = ['cap_max' => ['The maximum capacity cannot be less than or equal to the minimum capacity']];
                    }


            return $errores;
        }
    }

    public function ValidateDeleteTable($checkReserve,$identificator)
    {
        $data = array
        (
            'identificator' => $identificator
        );
        $rules = array
        (
            'identificator' => 'required|exists:tables,identificator',
        );

        $messages = array
        (
            'identificator.required' => 'The table identification number is necessary',
            'identificator.exists'   => 'This table dont exist in database, please check and try again'
        );

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails())
        {
            return $v->errors();
        }
        else
        {
            $errores = array();

            if($checkReserve > 0)
            {
                $errores = ['identificator' => ['This table cant delete: Exist a reservation']];
            }
            return $errores;
        }

    }
}