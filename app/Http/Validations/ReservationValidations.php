<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 27/3/2021
 * Time: 6:46 PM
 */

namespace App\Http\Validations;


use App\Http\Functionalities\ReserveFunctionalities;
use App\Models\Table;
use Illuminate\Support\Facades\Validator;

class ReservationValidations
{
    public function ValidateCreateReservation($request)
    {
        $data = $request->all();
        /*Using this way we validate that information is unique and isn't empty and format date*/

        $rules = array
        (
            'identificator' => 'required|unique:reservations,table_id',
            'client_name'   => 'required|max:25',
            'persons'       => 'required',
            'reserve_date'  => 'required|date_format:d-m-Y'
        );

        $messages = array
        (
            'identificator.required' => 'The table identification number is necessary',
            'identificator.unique' => 'It is not possible to reserve this table: It is already reserved',
            'client_name.required' => 'The client name is requerid',
            'client_name.max' => 'The client name must be 25 characters',
            'reserve_date.required' => 'The reserve date is requerid',
            'reserve_date.date_formtat' => 'The reserve date format must be DD-MM-YYYY',
            'persons.required' => 'The persons number is required'
        );

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails())
        {
            return $v->errors();
        }
        else
        {
            /*Using this other way we can create our own valid options*/
            $errores = array();
            $identificator = $request->identificator;
            $persons = $request->persons;


                /*Verify that the table to be reserved has sufficient capacity*/
                $table = Table::select('cap_max')
                    ->where('identificator','=',$identificator)
                    ->first();

                if($persons > $table->cap_max)
                {
                    $errores = ['identificator' => ['This table has a maximum capacity of: '.$table->cap_max.' and the client need a table for: '.$persons.' persons']];
                }

                return $errores;
        }
    }

    public function ValidateViewTablesToReserve($reserveDate, $persons)
    {

        $data = array
        (
            'persons' => $persons,
            'reserve_date' => $reserveDate
        );

        $rules = array
        (
            'persons'       => 'required|numeric',
            'reserve_date'  => 'required|date_format:d-m-Y'
        );

        $messages = array
        (
            'persons.required' => 'The persons number is required',
            'persons.numeric' => 'The persons must be a number',
            'reserve_date.required' => 'The reserve date is requerid',
            'reserve_date.date_format' => 'The reserve date format must be DD-MM-YYYY'

        );

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails())
        {
            return $v->errors();
        }
    }

    public function ValidateDeleteReserve($reserveNumber)
    {
        $data = array
        (
            'reserve_number' => $reserveNumber
        );

        $rules = array
        (
            'reserve_number'  => 'required|max:19|exists:reservations,reserve_number'
        );

        $messages = array
        (
            'reserve_number.required' => 'The reserve number is requerid',
            'reserve_number.exists' => 'The reserve number dont exist in database, please check and try again',
            'reserve_number.max' => 'The reserve number must be 19 characters',

        );

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails())
        {
            return $v->errors();
        }
    }
}