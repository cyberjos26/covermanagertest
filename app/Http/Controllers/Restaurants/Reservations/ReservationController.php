<?php

namespace App\Http\Controllers\Restaurants\Reservations;

use App\Http\Controllers\Controller;
use App\Http\Validations\ReservationValidations;
use App\Models\Reservation;
use App\Models\Table;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function CreateReservation(Request $request)
    {
        /*Apply validations rules*/
        $validationReservation = new ReservationValidations();
        $errors = $validationReservation->ValidateCreateReservation($request);

        if ($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
        {
            $identificator = $request->identificator;
            $clientName = $request->client_name;
            $persons = $request->persons;
            $reserveDate = Reservation::ConverISOFormat($request->reserve_date);
            $reserveNumber = Reservation::CreateReserveNumber();

            $reservation = new Reservation();
            $reservation->table_id = $identificator;
            $reservation->client_name = $clientName;
            $reservation->persons = $persons;
            $reservation->reserve_date = $reserveDate;
            $reservation->reserve_number = $reserveNumber;
            $reservation->save();

            return response()->json
            ([
                'status' => 'ok',
                'information_reserve' =>
                    ['reserve_number' => $reserveNumber]

            ], 200);



        }
    }

    public function CheckTableReservationPerDate($reserveDate,$persons)
    {

        /*Apply validations rules*/
        $validationReservation = new ReservationValidations();
        $errors = $validationReservation->ValidateViewTablesToReserve($reserveDate, $persons);

        if ($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
        {
            $reserveDateIso = Reservation::ConverISOFormat($reserveDate);

            $checkTableReservationPerDate = Table::select('tables.identificator','tables.cap_min','tables.cap_max')
                ->whereRaw("tables.identificator 
                NOT IN (SELECT reservations.table_id FROM reservations 
                WHERE reservations.reserve_date='$reserveDateIso')")
                ->where('tables.cap_max','>=',$persons)
                ->where('tables.cap_min','<=',$persons)
                ->get();

            return response()->json
            ([
                'status' => 'ok',
                'available_tables' => $checkTableReservationPerDate
            ], 200);
        }


    }

    public function DeleteReserve($reserveNumber)
    {
        /*Apply validations rules*/
        $validationReservation = new ReservationValidations();
        $errors = $validationReservation ->ValidateDeleteReserve($reserveNumber);

        if ($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
        {
            $deleteReserve = Reservation::select('reserve_number')
                ->where('reserve_number','=',$reserveNumber)
                ->delete();

            return response()->json
            ([
                'status' => 'ok',
            ], 200);

        }
    }
}
