<?php

namespace App\Http\Controllers\Restaurants\Tables;

use App\Http\Controllers\Controller;
use App\Http\Functionalities\ReserveFunctionalities;
use App\Http\Validations\TableValidations;
use App\Models\Reservation;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class TableController extends Controller
{

    public function CreateTable(Request $request)
    {

        /*Apply validations rules*/
        $validationTable = new TableValidations();
        $errors = $validationTable->ValidateCreateTable($request);

        if ($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
            {
                $identificator = $request->identificator;
                $capMin = $request->cap_min;
                $capMax = $request->cap_max;

                $table = new Table();
                $table->identificator = $identificator;
                $table->cap_min = $capMin;
                $table->cap_max = $capMax;
                $table->save();

                return response()->json
                ([
                    'status' => 'ok'
                ], 200);



            }
    }

    public function DeleteTable($tableIdenficator)
    {
        $validationTable = new TableValidations();
        $checkReservePerTable = new ReserveFunctionalities();

        /*Apply validations rules*/
        $check = $checkReservePerTable->CheckTableReservePerIdTable($tableIdenficator);
        $errors = $validationTable->ValidateDeleteTable($check,$tableIdenficator);

        if ($errors)
        {
            return response()->json
            ([
                'errors' => $errors
            ],422);
        }
        else
        {
            $deleteTable = Table::select('identificator')
                ->where('identificator','=',$tableIdenficator)
                ->delete();

            return response()->json
            ([
                'status' => 'ok'
            ], 200);
        }

    }


}