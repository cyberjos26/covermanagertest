<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reservation
 * 
 * @property int $id
 * @property int $table_id
 * @property string|null $reserve_number
 * @property string|null $client_name
 * @property int $persons
 * @property Carbon $reserve_day
 * 
 * @property Table $table
 *
 * @package App\Models
 */
class Reservation extends Model
{
	protected $table = 'reservations';
	public $timestamps = false;

	protected $casts = [
		'table_id' => 'int',
		'persons' => 'int'
	];

	protected $dates = [
		'reserve_date'
	];

	protected $fillable = [
		'table_id',
		'reserve_number',
		'client_name',
		'persons',
		'reserve_date'
	];

	public function table()
	{
		return $this->belongsTo('app\Table','identificator','table_id');
	}

    public static function  ConverISOFormat($date)
    {
        $date=Carbon::createFromFormat('d-m-Y',$date)->toDateString();
        return $date;
    }

    public static function CreateReserveNumber()
    {
        $reserveNumber = date('YmdHis').rand(10000,99000);
        return $reserveNumber;
    }

}
