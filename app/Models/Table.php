<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Table
 * 
 * @property int $id
 * @property int $identificator
 * @property int $cap_min
 * @property int $cap_max
 * 
 * @property Collection|Reservation[] $reservations
 *
 * @package App\Models
 */
class Table extends Model
{
	protected $table = 'tables';
	public $timestamps = false;

	protected $casts = [
		'identificator' => 'int',
		'cap_min' => 'int',
		'cap_max' => 'int'
	];

	protected $fillable = [
		'identificator',
		'cap_min',
		'cap_max'
	];

	public function reservations()
	{
		return $this->hasMany(Reservation::class);
	}
}
