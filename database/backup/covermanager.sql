-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3307
-- Tiempo de generación: 28-03-2021 a las 14:58:11
-- Versión del servidor: 10.3.12-MariaDB
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `covermanager`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) NOT NULL,
  `reserve_number` varchar(20) DEFAULT NULL,
  `client_name` varchar(25) DEFAULT NULL,
  `persons` int(11) NOT NULL,
  `reserve_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reserve_number_UNIQUE` (`reserve_number`),
  KEY `fk_table_id_reservations_idx` (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reservations`
--

INSERT INTO `reservations` (`id`, `table_id`, `reserve_number`, `client_name`, `persons`, `reserve_date`) VALUES
(4, 2, '2021032718451078030', 'Juan', 2, '2021-03-27'),
(7, 4, '2021032814114085899', 'Jose', 2, '2021-03-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tables`
--

DROP TABLE IF EXISTS `tables`;
CREATE TABLE IF NOT EXISTS `tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` int(11) NOT NULL,
  `cap_min` int(11) NOT NULL,
  `cap_max` int(11) NOT NULL,
  PRIMARY KEY (`id`,`identificator`),
  UNIQUE KEY `identificator_UNIQUE` (`identificator`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tables`
--

INSERT INTO `tables` (`id`, `identificator`, `cap_min`, `cap_max`) VALUES
(1, 4, 2, 3),
(2, 2, 2, 3),
(3, 3, 1, 5),
(5, 1, 1, 3),
(6, 5, 2, 5);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `fk_table_id_reservations` FOREIGN KEY (`table_id`) REFERENCES `tables` (`identificator`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
